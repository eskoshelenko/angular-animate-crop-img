import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { slideInOut } from './app.animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [ slideInOut ],
  providers: [ 
    FileReader, 
  ]
})
export class AppComponent implements OnInit {
  @ViewChild('canvas')
  canvas!: ElementRef
  localUrl = ''

  uploadForm!: FormGroup
  animations = [
    { direction: 'RL', name: 'Slide right to left' },
    { direction: 'LR', name: 'Slide left to right' },
    { direction: 'TB', name: 'Slide top to bottom' },
    { direction: 'BT', name: 'Slide bottom to top' },
  ]

  cropForm!: FormGroup
  isImg = false
  isClicked = false
  clientX!: number
  clientY!: number


  constructor (private reader: FileReader) {}
  ngOnInit(): void {
    this.uploadForm = new FormGroup({
      pathToFile: new FormControl('', Validators.required),
      clickUrl: new FormControl('', Validators.required),
      animation: new FormControl('', Validators.required),
      canvas: new FormControl(''),
    })
    this.cropForm = new FormGroup({
      height: new FormControl(100),
      width: new FormControl(100),
    })
    this.reader.onload = (event: any) => {
      this.localUrl = event.target.result
    }
  }

  onSelectFile(event: Event) {
    this.reader.readAsDataURL((<HTMLInputElement>event.target).files![0])
  }

  onDoubleClickImg (event: Event) {
    this.cropForm.setValue({
      height: 100,
      width: 100
    })
    const img = event.target as HTMLImageElement
    const canvas = this.canvas.nativeElement
    canvas.height = canvas.width * (img.height / img.width);
    const context: CanvasRenderingContext2D = this.canvas.nativeElement.getContext('2d')

    context.drawImage(img, 0 , 0, img.offsetWidth, canvas.height )
    this.isImg = true
  }

  onChangeOption() {
    const canvas = this.canvas.nativeElement
    const context: CanvasRenderingContext2D = this.canvas.nativeElement.getContext('2d')
    context.drawImage(
      canvas, 
      0, 
      0, 
      canvas.width * this.cropForm.get('width')?.value / 100, 
      canvas.height * this.cropForm.get('height')?.value / 100, 
      0, 
      0, 
      canvas.width, 
      canvas.height
    )
    this.cropForm.setValue({
      height: 100,
      width: 100
    })
  }

  onDoubleClickCanvas() {
    this.localUrl = this.canvas.nativeElement.toDataURL()
    this.isClicked = false
  }

  onMouseDownCanvas(event: MouseEvent) {
    this.isClicked = true
    const { clientX, clientY } = event
    this.clientX = clientX
    this.clientY = clientY
  }

  onMouseMoveCanvas(event: MouseEvent) {
    if (this.isClicked ) {
      const canvas = this.canvas.nativeElement
      const context: CanvasRenderingContext2D = this.canvas.nativeElement.getContext('2d')
      const { clientX, clientY } = event
      const dx = clientX - this.clientX
      const dy = clientY - this.clientY  
      this.clientX = clientX
      this.clientY = clientY

      context.drawImage(
        canvas, 
        dx, 
        dy, 
        canvas.width * this.cropForm.get('width')?.value / 100, 
        canvas.height * this.cropForm.get('height')?.value / 100
      );
    }
  }

  onMouseUpCanvas() {
    this.isClicked = false
  }
}
