import { animate, style, transition, trigger, state } from '@angular/animations';

export const slideInOut = trigger('slideInOut', [
  state('RL, LR', style({ transform: 'translateX(0%)' })),
  state('TB, BT', style({ transform: 'translateY(0%)' })),
  transition('* => RL', [
    style({ transform: 'translateX(-100%)' }),
    animate('400ms ease-in')
  ]),
  transition('* => LR', [
    style({ transform: 'translateX(100%)' }),
    animate('400ms ease-in')
  ]),
  transition('* => BT', [
    style({ transform: 'translateY(100%)' }),
    animate('400ms ease-in'),
  ]),
  transition('* => TB', [
    style({ transform: 'translateY(-100%)' }),
    animate('400ms ease-in'),
  ]),
])